/**
 * 
 */
package de.flexguse.vaadin.addon.spring.touchkit.demo;

import org.springframework.web.context.ConfigurableWebApplicationContext;

import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;

import de.flexguse.vaadin.addon.spring.touchkit.SpringTouchkitApplication;

/**
 * DemoApplication for mobile application just to show Spring works with a
 * Touchkit application.
 * 
 * @author Christoph Guse, info@flexguse.de
 * 
 */
public class DemoMobileApplication extends SpringTouchkitApplication {

    private static final long serialVersionUID = -1620529949585068850L;

    private String notificationText;

    /**
     * @return the notificationText
     */
    public String getNotificationText() {
	return notificationText;
    }

    /**
     * @param notificationText
     *            the notificationText to set
     */
    public void setNotificationText(String notificationText) {
	this.notificationText = notificationText;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.dellroad.stuff.vaadin.touchkit.SpringTouchkitApplication#
     * onBrowserDetailsReady()
     */
    @Override
    public void onBrowserDetailsReady() {

	Button button = new Button("click me");

	button.addListener(new Button.ClickListener() {

	    private static final long serialVersionUID = 1000238561242401395L;

	    @Override
	    public void buttonClick(ClickEvent event) {
		getMainWindow().showNotification(getNotificationText());

	    }
	});

	getMainWindow().addComponent(button);

    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * org.dellroad.stuff.vaadin.SpringContextApplication#initSpringApplication
     * (org.springframework.web.context.ConfigurableWebApplicationContext)
     */
    @Override
    protected void initSpringApplication(
	    ConfigurableWebApplicationContext context) {

	/*
	 * Initialise the application. Don't forget to run the init method in
	 * the super class, otherwise you get an error the Vaadin Application
	 * Window can't be found.
	 */
	super.initSpringApplication(context);

    }

}
