/**
 * 
 */
package de.flexguse.vaadin.addon.spring.touchkit;

import java.net.URL;
import java.util.Collection;
import java.util.Iterator;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.dellroad.stuff.vaadin.SpringContextApplication;
import org.springframework.web.context.ConfigurableWebApplicationContext;

import com.vaadin.addon.touchkit.ui.TouchKitApplication;
import com.vaadin.addon.touchkit.ui.TouchKitWindow;
import com.vaadin.service.ApplicationContext;
import com.vaadin.terminal.gwt.server.HttpServletRequestListener;
import com.vaadin.terminal.gwt.server.WebApplicationContext;
import com.vaadin.terminal.gwt.server.WebBrowser;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.BaseTheme;

/**
 * The {@link SpringTouchkitApplication} does the same like
 * {@link TouchKitApplication} does, in fact this is nearly a copy taken from
 * Touchkit addon 2.1.
 * 
 * The addition is this {@link TouchKitApplication} implementation works with
 * the Spring framework.
 * 
 * @author Christoph Guse, info@flexguse.de
 * 
 */
public abstract class SpringTouchkitApplication extends
	SpringContextApplication implements HttpServletRequestListener {

    private static final long serialVersionUID = 1568642764523250653L;

    private boolean browserDetailsReady = false;

    /**
     * Standard constructor setting the Base Theme.
     */
    public SpringTouchkitApplication() {
	setTheme(BaseTheme.THEME_NAME);
    }

    /**
     * Use this constructor if you want to set your custom theme directly.
     * 
     * @param theme
     *            the name of the custom theme.
     */
    public SpringTouchkitApplication(String theme) {
	setTheme(theme);
    }

    @Override
    protected void initSpringApplication(
	    ConfigurableWebApplicationContext context) {

	setMainWindow(new TouchKitWindow());
    }

    /**
     * Gets the active application instance for this thread. Allows one to do
     * <code>MyTouchKitApplication.get()</code> to get the application instance
     * anywhere in the UI code, provided we're in the request/response thread
     * (using the ThreadLocal pattern).
     * 
     * @return the active application instance
     */
    public static SpringTouchkitApplication get() {
	return (SpringTouchkitApplication) SpringTouchkitApplication
		.currentApplication();
    }

    /**
     * @see WebApplicationContext#getBrowser()
     */
    public WebBrowser getBrowser() {
	return ((WebApplicationContext) getContext()).getBrowser();
    }

    /**
     * UI building should happen when this method is called. At this point all
     * details in {@link WebBrowser} ({@link #getBrowser()}) is available - i.e
     * the type of device and screen size is known, allowing for decisions about
     * which type of UI to show. A {@link TouchKitWindow} is set as the main
     * window.
     */
    public abstract void onBrowserDetailsReady();

    @Override
    public void setMainWindow(Window mainWindow) {
	if (mainWindow instanceof TouchKitWindow) {
	    super.setMainWindow(mainWindow);
	} else {
	    throw new IllegalArgumentException("Only "
		    + TouchKitWindow.class.getSimpleName()
		    + " can be set as main window");
	}
    }

    @Override
    public TouchKitWindow getMainWindow() {
	return (TouchKitWindow) super.getMainWindow();
    }

    @Override
    public TouchKitWindow getWindow(String name) {
	return (TouchKitWindow) super.getWindow(name);
    }

    /**
     * Performs two tasks:
     * <ul>
     * <li>Waits for browser details to become available, then calls
     * {@link #onBrowserDetailsReady()}.</li>
     * <li>Sets the active application instance for this thread, allowing it to
     * be fetched within this reqest/response (or thread, more specifically)
     * using {@link TouchKitApplication#get()} (ThreadLocal pattern)</li>
     * </ul>
     * 
     * @see com.vaadin.terminal.gwt.server.HttpServletRequestListener#onRequestStart(javax.servlet.http.HttpServletRequest,
     *      javax.servlet.http.HttpServletResponse)
     */
    @Override
    protected void doOnRequestStart(HttpServletRequest request,
	    HttpServletResponse response) {

	super.doOnRequestStart(request, response);

	if (!browserDetailsReady) {
	    if (request.getParameter("repaintAll") != null) {
		int viewWidth = Integer.parseInt(request.getParameter("vw"));
		int viewHeight = Integer.parseInt(request.getParameter("vh"));
		Collection<Window> windows2 = getWindows();
		for (Iterator<Window> iterator = windows2.iterator(); iterator
			.hasNext();) {
		    TouchKitWindow window = (TouchKitWindow) iterator.next();
		    window.setWidth(viewWidth, TouchKitWindow.UNITS_PIXELS);
		    window.setHeight(viewHeight, TouchKitWindow.UNITS_PIXELS);
		}
		browserDetailsReady = true;
		if (isRunning()) {
		    onBrowserDetailsReady();
		}
	    }
	}

    }

    @Override
    public void start(URL applicationUrl, Properties applicationProperties,
	    ApplicationContext context) {
	super.start(applicationUrl, applicationProperties, context);
	if (browserDetailsReady) {
	    onBrowserDetailsReady();
	}
    }
}
